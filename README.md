
<!-- markdown-toc start - Don't edit this section. Run M-x markdown-toc-generate-toc again -->
**Table of Contents**

- [SynKit](#synkit)
- [Goals](#goals)
- [API Specifics](#api-specifics)
    - [Concerns](#concerns)
        - [Core Protocols](#core-protocols)
        - [Supporting Protocols](#supporting-protocols)
    - [CoreData Specifics](#coredata-specifics)
        - [Intialization](#intialization)
        - [Contextualization](#contextualization)
        - [Transactions](#transactions)
        - [Adoption](#adoption)
        - [UI Syncing](#ui-syncing)
            - [FetchedTableViewDataSource](#fetchedtableviewdatasource)
            - [Listen](#listen)
- [Sub APIS](#sub-apis)
    - [TextField Utilities](#textfield-utilities)
        - [TFFocus](#tffocus)
    - [Operators](#operators)
    - [WaitableViewController](#waitableviewcontroller)
    - [Promise Transitions](#promise-transitions)
    - [General Utilities](#general-utilities)
        - [Once](#once)
    - [Authentication](#authentication)
        - [KeyChain](#keychain)
            - [Operations](#operations)
    - [UIUtilities](#uiutilities)
        - [ArrayDataSource](#arraydatasource)
- [Current Deficiencies](#current-deficiencies)

<!-- markdown-toc end -->


# SynKit

SynKit from a high level is a library to help with syncing a rest web server and a CoreData stack

# Goals

* Provide abstract syncing concepts
* Keeping transfer method in mind i.e. HTTP (Dont forget about the wire!)
* Provide mechanisms to sync UI with model

# API Specifics

## Concerns 

SynKits core functionality is broken down into seperate concerns.
Concerns are aspects of syncing a model with a remote version.

### Core Protocols

* Syncable
* Fetchable
* Refreshable

Core Protocols are the focus of this api.
Creating a new or updating an old remote copy uses Syncable.
Reloading update remote data uses Refreshable.
Listing the remote objects uses Fetchable.

### Supporting Protocols

The Core Protocols, given above, provide a start with which to frame work.
There are still some finer details to decide.

If a model implements the following protocols and is a NSManagedObject, extensions are provided within
this api that will implement the rest of `Syncable`, `Fetchable`, and `Refreshable`.

* ModalType
* Inflatable
* Encodable
* Routable

## CoreData Specifics

Eventually it would be nice if this api didn't have to work specifically with CoreData.
But we're using it to get this framework working fast, and it's not a bad base with which to start.

### Intialization

This api requires it to be initialized most likely in the app delegate,
To initialize this api, you must call the initialize function with a NSManagedObjectContext and an API Mode.
This NSManagedObjectContext is known as the `rootContext` inside of the api.

This design is flexable to have another context under the `rootContext`, but if done the user must specify
syncing to the local data store by saving the contexts under the `rootContext`. 

API Modes are Production and Debug. The difference being debug will print curl representations of http requests
whether they contain passwords or not!

The `rootContext` is called `rootContext` for a reason, inside the api the implementations of the Core Protocols are
allowed and encouraged to create child contexts to do there work.


### Contextualization

Each of the Core Protocols involve a `NSManagedObjectContext` in order to modify local copies to reflect the 
remote copies.

Since the Core Protocols are dependant upon `NSManagedObjectContext`s, the caller must provide one.
This is where `DataContext` becomes useful.
`DataContext` is a protocol which provides a `NSManagedObjectContext` through `dbContext`.
All UIViewControllers are `DataContext`s which provide the `rootContext`, for this reason it's important that the `rootContext` uses the main thread.
If you want a general context, simply implement the `WorkerDataContext` protocol to get `dbContext` that is a child of the `rootContext` 

### Transactions

`DataTransaction` is a way of ordering and executing `NSManagedObjectContext` changes.
A consequence of this is to a parent `NSManagedObjectContext` changes are atomic.
`DataTransaction` has utilities to work with promises, `DataTransaction`s are also asynchronous.

`DataTransaction` has a subclass `SubjectDataTransaction` which adds a Subject which can be mutated.

### Adoption

With `NSManagedObjectContext`s mutations must happen in their own thread and no other.
To help with transitioning context an extention and helper class help with this.

A `NSManagedObjectContext` can adopt a `NSManagedObject` which actual looks up the object in itself.
This works well with adopting "down stream" or by child contexts, but child context must save objects in order for parents to adopt child contexts objects.

`Adoption` is a struct that provides the mechanism to adopt like above, but does not allow access to the source context

### UI Syncing

#### FetchedTableViewDataSource

`FetchedTableViewDataSource` is a `UITableViewDataSource` which relates to a `NSFetchRequest`, this data source reacts to updates to the core data objects in real time. 
A `NSPredicate` may be set on this data source to detail a more specific query, which is handy in domains such as search.
A `selectedObject` is possibly the corrisponding object to the row that is selected.

Unfortunately `FetchedTableViewDataSource` is not generically typed currently, it has been in the past but this created some what of 
an unweildy use of it (see also Protocols with Type Associations). There is a possibility of this data source being typed in the future.

Eventually a `UICollectionViewDataSource` version will be written if it's applicable.

#### Listen

`NSManagedObject`s can be listened to for local changes, UIs can alter themselves with this mechanism.

# Sub APIS

Not all of these are apis in their own right. But there can be more then one.

## TextField Utilities

### TFFocus

TFFocus provides a focus group of `UITextField`s, there is an order.
When you press return on one, it will resign focus and call focus to the next.
If the last one has return pressed, an action specified on creation is triggered.

## Operators
`|>` Function application

Example
```
1 |> inc // 2
```
`>>>` Function composition

Example
```
let mx_plus_b = double >>> inc

mx_plus_b(1) //3
```


## WaitableViewController

`WaitableViewController` exposes two core methods and one general one.

* wait
* unwait
* waitFor

`wait` increments a wait count.
`unwait` decrements the wait count.

when the wait count is not at zero the view controllers view will be greyed out, uninteractable, and show an activity indicator.

`waitFor` incorperates the prior methods with a promise.

## Promise Transitions 

`UIViewController` has an extension which works with promises to do UIActions

* Segue
* Alert
* Change Storyboards
* Dismiss (pop to root of `UINavigationViewController`)

## General Utilities

### Once

`once` is a function which takes a function with a signature of `() -> ()` or `T -> ()` and
results in a function with the same signature, which the original implemenation may only
be called once.

## Authentication

### KeyChain 

`KeyChain` is an abstraction on top of the regular iOS keychain.

#### Operations

* `savePassword(_: String, forUsername: String) throws` saves a username password pair
* `fetchPasswordForUsername(_: String) throws -> String?` retrieves a store password for a user
* `deletePasswordForUsername(_: String) throws` deletes a username password pair

Any of these methods may throw a `KeyChainError` which is just an abstraction over a keychain error

## UIUtilities

### ArrayDataSource

An adatper to use an array as a backing store for a tableview.

# Current Deficiencies

* Currently has other libraries inside of it.
* Only works with json.
* Has only been proven with a Rails Server.
* Must work with CoreData, it would be nice to work with other storage engines (files, nsuserdefaults).
* Never tested for memory cycles or used other profiling technics
