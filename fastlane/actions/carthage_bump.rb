module Fastlane
  module Actions
    module SharedValues
      CARTHAGE_BUMP_CUSTOM_VALUE = :CARTHAGE_BUMP_CUSTOM_VALUE
    end

    # To share this integration with the other fastlane users:
    # - Fork https://github.com/fastlane/fastlane/tree/master/fastlane
    # - Clone the forked repository
    # - Move this integration into lib/fastlane/actions
    # - Commit, push and submit the pull request

    class CarthageBumpAction < Action
			class GitTag
				attr_reader :major, :minor, :patch

				def initialize(str)
					@major, @minor, @patch = str.gsub(/[A-Za-z]*/, "").split(".").map(&:to_i)

					@major ||= 0
					@minor ||= 0
					@patch ||= 0
				end
				
				def increment(type)
					case type
					when :patch
						@patch = @patch + 1
					when :minor
						@patch = 0
						@minor = @minor + 1
					when :major
						@patch = 0
						@minor = 0
						@major = @major + 1
					end
				end

				def <=>(other)
					result = major <=> other.major
					result = minor <=> other.minor unless result != 0
					result = patch <=> other.patch unless result != 0
					result
				end

				def to_s
					"#{major}.#{minor}.#{patch}"
				end
			end


      def self.run(params)
				tags = `git tag`.split.map { |str| GitTag.new str } .sort
				last = tags.last

				last.increment(params[:type])
				
				`git tag #{last.to_s}`
				last
      end

      #####################################################
      # @!group Documentation
      #####################################################

      def self.description
        "Increments the carthage version of the repo"
      end

      def self.details
        # Optional:
        # this is your chance to provide a more detailed description of this action
        "You can use this action to do cool things..."
      end

      def self.available_options
				[
					FastlaneCore::ConfigItem.new(key: :type,
																			 env_name: "FL_CARTHAGE_BUMP_TYPE",
																			 description: "Type of increament",
																			is_string: false)
				]
      end

      def self.output
        # Define the shared values you are going to provide
        # Example
        [
          ['CARTHAGE_BUMP_CUSTOM_VALUE', 'A description of what this value contains']
        ]
      end

      def self.return_value
        # If you method provides a return value, you can describe here what it does
      end

      def self.authors
        # So no one will ever forget your contribution to fastlane :) You are awesome btw!
        ["felgenh3"]
      end

      def self.is_supported?(platform)
				[:ios, :mac].include? platform
      end
    end
  end
end
