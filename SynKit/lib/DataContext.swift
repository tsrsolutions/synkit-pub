//
//  DataContext.swift
//  SynKit
//
//  Created by Maximilian Felgenhauer on 4/7/16.
//  Copyright © 2016 Maximilian Felgenhauer. All rights reserved.
//

import Foundation
import CoreData

public protocol DataContext: class {
	/**
	Returns an appropriate database context for the work a class
	*/
	var dbContext: NSManagedObjectContext { get }
}

public protocol WorkerDataContext: DataContext { }