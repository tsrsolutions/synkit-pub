//
//  File.swift
//  SynKit
//
//  Created by Maximilian Felgenhauer on 4/7/16.
//  Copyright © 2016 Maximilian Felgenhauer. All rights reserved.
//

/**

A protocol that marks a nsmanagedobject as being able to 
be used within SynKit

- Author: Maximilian Werner Felgenhauer
*/
public protocol ModelType: class {
	
	/**
	stores the remoteId of a given Model
	
	- Requires: The Corresponding entity to have a remoteId with type Optional<Int64>
	*/
	var remoteId: Int64? { get set }
	
	/**
	Describes the name of an entity
	
	- Returns: the name of a entity in a CoreData model
	- Requires: A CoreData Entity which this value names.
	*/
	static var entityName: String { get }
}