//
//  Refreshable.swift
//  SynKit
//
//  Created by Maximilian Felgenhauer on 4/7/16.
//  Copyright © 2016 Maximilian Felgenhauer. All rights reserved.
//

import Foundation
import CoreData
import PromiseKit

public protocol Refreshable: RoutableEntity, Inflatable, ModelType {
	/**
	Attempts to refresh the local copy with the remote copy
	
	- Parameter context: a context to update self
	- Returns: A Promise when self is updated
	- Remark: Should call HTTP method get on its `route`
	*/
	func refresh(inContext context:NSManagedObjectContext) -> Promise<Self>
}
