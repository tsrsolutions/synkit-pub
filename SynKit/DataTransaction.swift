//
//  Transaction.swift
//  SynKit
//
//  Created by Maximilian Felgenhauer on 4/7/16.
//  Copyright © 2016 Maximilian Felgenhauer. All rights reserved.
//

import Foundation
import PromiseKit
import CoreData

open class DataTransaction {
	open var operation: Promise<Void>
	let context: NSManagedObjectContext
	
	fileprivate var onErrors = Array<(Error) -> ()>()
	var err: Error? {
		didSet {
			if let e = err {
				for cb in onErrors {
					cb(e)
				}
			}
		}
	}
	
	init(context: NSManagedObjectContext) {
		self.context = context
		self.operation = Promise(value: ())
	}
	
	open func fetchAll<T: NSManagedObject>(_ type: T.Type, withParameters parameters: [String:String] = [:]) where T: Fetchable {
		run {
			return type.fetch(inContext: self.context, withParameters: parameters)
		}
	}
	
	open func onMain(_ fn: @escaping () -> ()) {
		operation = operation.always(on: DispatchQueue.main, execute: fn)
	}
	
	open func onError(_ fn: @escaping (Error) -> ()) {
		onErrors.append(fn)
	}
	
	open func doWork<T>(_ fn: @escaping (NSManagedObjectContext) throws -> T) -> Promise<T> {
		return Promise { resolve, reject in
			self.run {
				do {
					resolve(try fn(self.context))
				} catch let err {
					self.err = err
					reject(err)
				}
			}
		}
	}
	
	open func doWork(_ fn: @escaping (NSManagedObjectContext) throws -> ()) {
		self.run {
			do {
				try fn(self.context)
			} catch let err {
				self.err = err
			}
		}
	}
	
	open func doWork<T>(_ fn: @escaping (NSManagedObjectContext) throws -> Promise<T>) -> Promise<T> {
		return Promise { resolve, reject in
			self.run { () -> Promise<T> in
				do {
					let p = try fn(self.context)
					p.settle(resolve: resolve, error: reject)
					return p
				} catch let err {
					self.err = err
					reject(err)
					
					return Promise(error: err)
				}
			}
		}
	}
	
	open func waitFor<T>(_ promise: Promise<T>) {
		run {
			return promise
		}
	}
	
	open func end() {
		run {
			do {
				try self.context.save()
				do {
					try self.context.parent?.save()
				} catch let err {
					print(err)
					self.err = err
					self.context.rollback()
				}
			} catch let err {
				print(err)
				self.err = err
				self.context.rollback()
			}
		}
	}
	
	func run(_ fn: @escaping () -> ()) {
		operation = operation.then { _ in
			Promise { resolve, reject in
				fn()
				resolve(())
			}
		}
	}
	
	func run<T>(_ fn: @escaping () -> Promise<T>) {
		operation = operation.then { _ in
			return fn().asVoid()
		}
	}
}
