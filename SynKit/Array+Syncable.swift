//
//  Array+Syncable.swift
//  SynKit
//
//  Created by Maximilian Felgenhauer on 4/7/16.
//  Copyright © 2016 Maximilian Felgenhauer. All rights reserved.
//

import Foundation
import CoreData
import PromiseKit

public extension Array where Element: Syncable {
	public func sync(inContext context: NSManagedObjectContext) -> Promise<Array<Element>> {
		var ops = Array<Promise<Element>>()
		
		for e in self {
			ops.append(e.sync(nil, inContext: context))
		}
		
		return when(fulfilled: ops)
	}
}
