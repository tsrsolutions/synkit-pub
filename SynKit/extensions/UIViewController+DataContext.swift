//
//  UIViewController+DataContext.swift
//  SynKit
//
//  Created by Maximilian Felgenhauer on 4/7/16.
//  Copyright © 2016 Maximilian Felgenhauer. All rights reserved.
//

import Foundation
import UIKit
import CoreData

extension UIViewController: DataContext {
	public var dbContext: NSManagedObjectContext {
		guard let context = rootContext else {
			fatalError("Failed to initialize app with a root context")
		}
		
		return context
	}
}

extension UIView: DataContext {
	public var dbContext: NSManagedObjectContext {
		guard let context = rootContext else {
			fatalError("Failed to initialize app with a root context")
		}
		
		return context
	}
}
