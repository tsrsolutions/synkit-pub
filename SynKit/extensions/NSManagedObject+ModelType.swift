//
//  NSManagedObject+ModelType.swift
//  SynKit
//
//  Created by Maximilian Felgenhauer on 4/7/16.
//  Copyright © 2016 Maximilian Felgenhauer. All rights reserved.
//

import Foundation
import CoreData

public extension ModelType where Self: NSManagedObject {
	public static var sortDescriptors: [NSSortDescriptor] {
		return []
	}
	
	public static var query: NSFetchRequest<Self> {
		get {
			let query = NSFetchRequest<Self>(entityName: entityName)
			query.sortDescriptors = sortDescriptors
			return query
		}
	}
	
	public func queryRelationship<T: NSManagedObject>(_ name: String) -> NSFetchRequest<T> {
		print(self.entity.relationshipsByName)
		
		guard let relationship = self.entity.relationshipsByName[name] else {
			fatalError("No child with name \(name)")
		}
		
		guard let childEntity = relationship.destinationEntity else {
			fatalError("Entity should exist")
		}
		
		guard let inverse = relationship.inverseRelationship else {
			fatalError("This functionallity requires inverse relationships")
		}
		
		
		let req = NSFetchRequest<T>(entityName: childEntity.name!)
		req.sortDescriptors = Self.sortDescriptors
		req.predicate = NSPredicate(format: "%K == %@", argumentArray: [inverse.name, self])
		
		return req
	}
	
	public var remoteId: Int64? {
		get {
			let id = self.value(forKeyPath: "remoteId") as? NSNumber
			return id?.int64Value
		}
		set {
			let val = newValue.map { NSNumber(value: $0 as Int64) }
			setValue(val, forKeyPath: "remoteId")
		}
	}
	
	public static func create(_ context: NSManagedObjectContext, withRemoteId remoteId: Int64) -> Self {
		let inserted =  NSEntityDescription.insertNewObject(forEntityName: entityName, into: context)
		
		guard let new = inserted as? Self else {
			fatalError("Returned object \(inserted) was not an instance of \(entityName)")
		}
		
		new.remoteId = remoteId
		
		return new
	}
	
	public static func create(_ context: NSManagedObjectContext) -> Self {
		let inserted =  NSEntityDescription.insertNewObject(forEntityName: entityName, into: context)
		
		guard let new = inserted as? Self else {
			fatalError("Returned object \(inserted) was not an instance of \(entityName)")
		}
		
		return new
	}
	
	public static func findOrCreate(_ context: NSManagedObjectContext, withRemoteId remoteId: Int64, customize: (Self)->Void = { _ in }) -> Self {
		guard let result = find(context, remoteId: remoteId) else {
			
			let new = Self.create(context, withRemoteId: remoteId)
			customize(new)
			return new
		}
		
		return result
	}
	
	public static func findOrCreate(_ context: NSManagedObjectContext, withPredicate predicate: NSPredicate, customize: (Self) -> Void = { _ in }) -> Self {
		let request = NSFetchRequest<Self>(entityName: Self.entityName)
		
		request.fetchLimit = 1
		request.predicate = predicate
		
		guard let results = try? context.fetch(request) else {
			fatalError()
		}
		
		if let result = results.first {
			return result
		} else {
			let new = Self.create(context)
			customize(new)
			return new
		}
	}
	
	public static func find(_ context: NSManagedObjectContext, remoteId: Int64) -> Self? {
		return find(context, withPredicate: NSPredicate(format: "%@ == remoteId", argumentArray: [NSNumber(value: remoteId as Int64)]))
	}
	
	public static func find(_ context: NSManagedObjectContext, withPredicate predicate: NSPredicate) -> Self? {
		let q = query
		q.fetchLimit = 1
		q.predicate = predicate
		
		do {
			return try context.fetch(q).first
		} catch let err {
			print(err)
			return nil
		}
	}
	
	public func relationship<T: NSManagedObject>(_ name: String) throws -> NSFetchRequest<T> {
		guard let relationship = entity.relationshipsByName[name] else {
			throw RelationshipError.notFound
		}
		
		guard let target = relationship.destinationEntity?.name else {
			throw RelationshipError.ambiguousRelationship
		}
		
		guard relationship.isToMany else {
			throw RelationshipError.toOne
		}
		
		guard let inverse = relationship.inverseRelationship else {
			throw RelationshipError.unidirectional
		}
		
		let request = NSFetchRequest<T>(entityName: target)
		request.predicate = NSPredicate(format: "%@ == \(inverse.name)", self)
		return request
	}
	
	public func add(_ name: String, model: ModelType) throws {
		guard let relationship = entity.relationshipsByName[name] else {
			throw RelationshipError.notFound
		}
		
		guard relationship.isToMany else {
			throw RelationshipError.toOne
		}
		
		mutableSetValue(forKeyPath: name).add(model)
	}
}

public enum RelationshipError: Error {
	case notFound
	case toOne
	case unidirectional
	case ambiguousRelationship
}
