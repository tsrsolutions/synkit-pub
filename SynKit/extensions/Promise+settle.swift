//
//  Promise+settle.swift
//  SynKit
//
//  Created by Maximilian Felgenhauer on 4/7/16.
//  Copyright © 2016 Maximilian Felgenhauer. All rights reserved.
//

import Foundation
import PromiseKit

extension Promise {
	func settle(resolve: @escaping (T) -> (), error: @escaping (Error) -> ()) {
		self.then(on: .global(), execute: resolve).catch(execute: error)
	}
}
