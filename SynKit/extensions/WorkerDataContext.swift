//
//  WorkerDataContext.swift
//  SynKit
//
//  Created by Maximilian Felgenhauer on 4/7/16.
//  Copyright © 2016 Maximilian Felgenhauer. All rights reserved.
//

import Foundation
import CoreData

private var workerContextPtr: Int = 0

public extension WorkerDataContext {
	var dbContext: NSManagedObjectContext {
		guard let context = rootContext else {
			fatalError("Failed to initialize app with a root context")
		}
		
		if let ctx = objc_getAssociatedObject(self, &workerContextPtr) {
			return ctx as! NSManagedObjectContext
		}
		
		let ctx = context.subcontext
		
		objc_setAssociatedObject(self, &workerContextPtr, ctx, .OBJC_ASSOCIATION_RETAIN)
		
		return ctx
	}
}