//
//  NSManagedObject+Refreshable.swift
//  SynKit
//
//  Created by Maximilian Felgenhauer on 4/7/16.
//  Copyright © 2016 Maximilian Felgenhauer. All rights reserved.
//

import Foundation
import CoreData
import PromiseKit
import Alamofire

public extension Refreshable where Self: NSManagedObject, Self: RoutableCollection {
	public func refresh(inContext context: NSManagedObjectContext) -> Promise<Self> {
		let req = request(route, method: .get)
		
		if mode == .debug {
			print(req.debugDescription)
		}
		
		return req.responseJson().then { obj in
			return Promise { resolve, reject in
				context.perform {
					let me = context.adopt(self)!
					do {
						try me.inflate(obj)
						print(obj)
						resolve(me)
					} catch let err {
						reject(err)
					}
				}
			}
		}
	}
	
	public static func findBy(_ remoteId: Int64, inContext context: NSManagedObjectContext) -> Promise<Self> {
		
		if let obj = Self.find(context, remoteId:  remoteId) {
			return Promise(value: obj)
		}
		
		let req = request(Self.collectionRoute + remoteId, method: .get)
		if mode == .debug {
			print(req.debugDescription)
		}
		
		return req.responseJson().then(execute: process(context))
	}
	
	fileprivate static func process(_ context: NSManagedObjectContext) -> (Any) -> Promise<Self> {
		return { obj in
			let transaction = context.create(self)
			transaction.mutateSubject { (subject: Self) -> () in
				let id = try identifableEntity(obj)
				try subject.inflate(obj)
				
				subject.remoteId = id
			}
			
			return transaction.endTransaction()
		}
	}
}

