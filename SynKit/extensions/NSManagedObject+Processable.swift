//
//  NSManagedObject+Processable.swift
//  SynKit
//
//  Created by Maximilian Felgenhauer on 4/7/16.
//  Copyright © 2016 Maximilian Felgenhauer. All rights reserved.
//

import Foundation
import CoreData
import PromiseKit

public extension Processable where Self: Inflatable, Self: HasCollectionName, Self: ModelType, Self: NSManagedObject {
	static func processMany(_ context: NSManagedObjectContext) -> (Any) -> Promise<Array<Self>> {
		return { obj in
			let transaction = context.begin()
			defer { transaction.end() }
			
			return transaction.doWork(actualProcess(obj))
		}
	}
	
	fileprivate static func actualProcess(_ obj: Any) -> (NSManagedObjectContext) throws -> Array<Self> {
		return { context in
			return try namedCollection(collectionName)(obj).map { part in
				let id = try identifableEntity(part)
				let entity = Self.findOrCreate(context, withRemoteId: id)
				try entity.inflate(part)
				
				return entity
			}
		}
	}
}
