//
//  NSManagedObjectContext.swift
//  SynKit
//
//  Created by Maximilian Felgenhauer on 4/7/16.
//  Copyright © 2016 Maximilian Felgenhauer. All rights reserved.
//

import CoreData
import PromiseKit

public extension NSManagedObjectContext {
	public func transact(_ action: @escaping (NSManagedObjectContext) -> Promise<Bool>) {
		let ctx = subcontext
		ctx.perform {
			action(ctx).then { suc -> Bool in
				guard suc else {
					ctx.rollback()
					return false
				}
				
				try ctx.save()
				try ctx.parent?.save()
				
				return true
				
				}.catch { err in
					print(err)
					ctx.rollback()
			}
		}
	}
	
	public var subcontext: NSManagedObjectContext {
		let ctx = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
		ctx.parent = self
		return ctx
	}
}
