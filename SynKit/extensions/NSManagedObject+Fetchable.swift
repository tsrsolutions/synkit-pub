//
//  NSManagedObject+Fetchable.swift
//  SynKit
//
//  Created by Maximilian Felgenhauer on 4/7/16.
//  Copyright © 2016 Maximilian Felgenhauer. All rights reserved.
//

import CoreData
import PromiseKit
import Alamofire

public extension Fetchable where Self: NSManagedObject {
	public static func fetch(inContext context: NSManagedObjectContext, withParameters parameters: [String:String] = [:]) -> Promise<[Self]> {
		let req = request(Self.collectionRoute, method: .get, parameters: parameters)
		
		if mode == .debug {
			print(req.debugDescription)
		}
		
		return req.responseJson().then(execute: extract(context))
	}
	
	static func extract(_ context: NSManagedObjectContext) -> (Any) -> Promise<[Self]> {
		return { payload in
			return Promise { resolve, reject in
				do {
					let collection = try namedCollection(collectionName)(payload)
					
					context.perform {
						var result = [Self]()
						for obj in collection {
							do {
								let id = try identifableEntity(obj)
								let instance = Self.findOrCreate(context, withRemoteId: id)
								try instance.inflate(obj)
								result.append(instance)
							} catch let err { 
								reject(err)
							}
						}
						resolve(result)
					}
				} catch let err {
					reject(err)
				}
			}
		}
	}
}

