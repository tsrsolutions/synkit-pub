//
//  NSManagedObjectContext+transaction.swift
//  SynKit
//
//  Created by Maximilian Felgenhauer on 4/7/16.
//  Copyright © 2016 Maximilian Felgenhauer. All rights reserved.
//

import Foundation
import CoreData

public extension NSManagedObjectContext {
	public func begin() -> DataTransaction {
		let subcontext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
		subcontext.parent = self
		//		subcontext.mergePolicy = mergePolicy
		return DataTransaction(context: subcontext)
	}
	
	public func create<T: ModelType>(_ modelType: T.Type) -> SubjectDataTransaction<T> where T: NSManagedObject {
		let subcontext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
		subcontext.parent = self
		//		subcontext.mergePolicy = self.mergePolicy
		var subject: T!
		subcontext.performAndWait { () -> Void in
			subject = modelType.create(subcontext)
		}
		
		return SubjectDataTransaction(subject: subject, context: subcontext)
	}
	
	public func mutate<T: ModelType>(_ model: T) -> SubjectDataTransaction<T> where T: NSManagedObject {
		let subcontext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
		subcontext.parent = self
		//		subcontext.mergePolicy = self.mergePolicy
		
		let subject = subcontext.adopt(model)
		return SubjectDataTransaction(subject: subject!, context: subcontext)
	}
}
