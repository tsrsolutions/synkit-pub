//
//  Routable.swift
//  SynKit
//
//  Created by Maximilian Felgenhauer on 4/7/16.
//  Copyright © 2016 Maximilian Felgenhauer. All rights reserved.
//

import Foundation
import CoreData

/**
Declares that a model has a corresponding route 
for this entity

- SeeAlso: route
*/
public protocol RoutableEntity {
	/**
	- Returns: a route for the current entity
	*/
	var route: URL { get }
}

/**
Declares a route for the collection of the type

- SeeAlso: collectionRoute
*/
public protocol RoutableCollection {
	/** 
	- Returns: a route for the this collection of entities
	*/
	static var collectionRoute: URL { get }
}


public extension RoutableEntity where Self: RoutableCollection, Self: ModelType {
	public var route: URL {
		guard let id = remoteId else {
			fatalError("Routable Model Type: \(Self.entityName) doesn't have id")
		}
		
		return Self.collectionRoute.appendingPathComponent("\(id)")
	}
}
