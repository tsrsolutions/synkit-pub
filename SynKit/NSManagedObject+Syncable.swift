//
//  NSManagedObject+Syncable.swift
//  SynKit
//
//  Created by Maximilian Felgenhauer on 4/7/16.
//  Copyright © 2016 Maximilian Felgenhauer. All rights reserved.
//

import Foundation
import CoreData
import PromiseKit
import Alamofire


public extension Syncable where Self: NSManagedObject, Self: ModelType {
	public func sync(_ fields: [String]? = nil, inContext context: NSManagedObjectContext) -> Promise<Self> {
		if remoteId == nil {
			return create(inContext: context)
		}else {
			return update(fields, inContext: context)
		}
	}
	
	fileprivate func update(_ fields: [String]?, inContext context: NSManagedObjectContext) -> Promise<Self> {
		let payload = fields != nil ? encoded.select(fields!) : encoded
		
		let req = request(route, method: .put, parameters: payload, encoding: JSONEncoding.default)
		
		if mode == .debug {
			print(req.debugDescription)
		}
		
		return req.responseJson().then(execute: extract(context))
	}
	
	fileprivate func create(inContext context: NSManagedObjectContext) -> Promise<Self> {
		let req = request(Self.collectionRoute, method: .post, parameters: encoded, encoding: JSONEncoding.default)
		
		if mode == .debug {
			print(req.debugDescription)
		}
		
		return req.responseJson().then(execute: extract(context))
	}
	
	fileprivate func extract(_ context: NSManagedObjectContext) -> (Any) -> Promise<Self> {
		return { obj in
			return Promise { resolve, reject in
				
				context.perform {
					do {
						let id = try identifableEntity(obj)
						
						try self.inflate(obj)
						self.setValue(NSNumber(value: id as Int64), forKeyPath: "remoteId")
						try context.save()
						resolve(self)
					} catch let err {
						reject(err)
					}
				}
			}
		}
	}
}
