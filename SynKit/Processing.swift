//
//  Processing.swift
//  SynKit
//
//  Created by Maximilian Felgenhauer on 4/7/16.
//  Copyright © 2016 Maximilian Felgenhauer. All rights reserved.
//

import Foundation

public func namedCollection(_ name: String) -> (Any) throws -> [AnyObject] {
	return { obj in
		guard let dict = obj as? [String: AnyObject] else {
			throw DecodeError.typeMismatch(expected: "Dictionary", actual: "Any")
		}
		
		guard let collection = dict[name] else {
			throw DecodeError.missingKey(name)
		}
		
		guard let arr = collection as? [AnyObject] else {
			throw DecodeError.typeMismatch(expected: "Array", actual: collection.description)
		}
		
		return arr
	}
}

public func identifableEntity(_ obj: Any) throws -> Int64 {
	guard let dict = obj as? [String: AnyObject] else {
		throw DecodeError.typeMismatch(expected: "Dictionary", actual: "Any")
	}
	
	guard let idObject = dict["id"] else {
		throw DecodeError.missingKey("id")
	}
	
	guard let id = idObject as? NSNumber else {
		throw DecodeError.typeMismatch(expected: "Number", actual: idObject.description)
	}
	
	return id.int64Value
}


public enum DecodeError: Error {
	case missingKey(String)
	case typeMismatch(expected: String, actual: String)
}
