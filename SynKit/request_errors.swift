//
//  request_errors.swift
//  SynKit
//
//  Created by Maximilian Felgenhauer on 9/30/16.
//  Copyright © 2016 Maximilian Felgenhauer. All rights reserved.
//

import Foundation

public protocol HTTPError: Error {
	var reason: Error { get }
}

public struct StatusCodeError: HTTPError {
	public let actual: Int
	public let reason: Error
}

public struct HeaderError: HTTPError {
	public let header: String
	public let reason: Error
}

public struct ContentError: HTTPError {
	public let content: Data
	public let reason: Error
}

public struct WrappedHTTPError: HTTPError {
	public let reason: Error
}

public struct DescribableError: Error, CustomStringConvertible {
	public let description: String
	
	public init(_ message: String) {
		self.description = message
	}
}

public struct ExpectedValueError<T>: Error {
	let actual: T
	let expected: T
	
	init(expected: T, actual: T) {
		self.expected = expected
		self.actual = actual
	}
}

public protocol HTTPExpectation {
	func expect(data: Data?, response: HTTPURLResponse) throws
}

public struct ContentHTTPExpectation: HTTPExpectation {
	let type: String
	
	public enum ContentType {
		case json
		case other(String)
		
		var value: String {
			switch self {
			case .json:
				return "json"
			case let .other(str):
				return str
			}
		}
	}

	public init(type: ContentType) {
		self.type = type.value
	}
	
	public func expect(data: Data?, response: HTTPURLResponse) throws {
		guard let content = response.allHeaderFields["Content-Type"] as? String else {
			throw HeaderError(header: "Content-Type", reason: DescribableError("Empty Value"))
		}
		
		guard let mime = content.components(separatedBy: ";").first else {
			throw HeaderError(header: "Content-Type", reason: DescribableError("Missing mimetype"))
		}
		
		guard mime.contains(type) else {
			throw HeaderError(header: "Content-Type", reason: ExpectedValueError(expected: type, actual: content))
		}
	}
}

public struct StatusRangeExpectation: HTTPExpectation {
	let range: Range<Int>
	
	public init(_ range: Range<Int>) {
		self.range = range
	}
	
	public func expect(data: Data?, response: HTTPURLResponse) throws {
		guard range.contains(response.statusCode) else {
			throw StatusCodeError(actual: response.statusCode, reason: DescribableError("Did not fit in range \(range)"))
		}
	}
}


