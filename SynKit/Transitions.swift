//
//  Transitions.swift
//  SynKit
//
//  Created by Maximilian Felgenhauer on 4/13/16.
//  Copyright © 2016 Maximilian Felgenhauer. All rights reserved.
//

import Foundation
import UIKit
import PromiseKit



public extension UIViewController {
	
	//MARK: Segue
	
	public func transitionTo<T: AnyObject>(_ id: String) -> (_ p: Promise<T>) -> Promise<T> {
		return { (p: Promise<T>) in
			return p.then(on: DispatchQueue.main) { [weak self] (t: T) -> T in
				self?.performSegue(withIdentifier: id, sender: t)
				return t
			}
		}
	}
	
	public func transitionTo(_ id: String) -> (Promise<()>) -> Promise<()> {
		return { p in
			p.then(on: DispatchQueue.main) { [weak self] in
				self?.performSegue(withIdentifier: id, sender: nil)
			}
		}
	}
	
	//MARK: Alerts
	
	public func presentAlert(_ alert: UIAlertController) -> (Promise<()>) -> Promise<()> {
		return { p in
			return p.then(on: DispatchQueue.main) { [weak self] in
				self?.present(alert, animated: true, completion: nil)
			}
		}
	}
	
	public func presentAlert<T>(_ alert: UIAlertController) -> (Promise<T>) -> Promise<T> {
		return { p in
			p.then(on: DispatchQueue.main) { [weak self] (t: T) -> T in
				self?.present(alert, animated: true, completion: nil)
				return t
			}
		}
	}
	
	//MARK: Storyboards
	
	public func transitionToStoryboard(_ board: UIStoryboard) -> (Promise<()>) -> Promise<()> {
		return { p in
			return p.then(on: DispatchQueue.main) { () -> () in
				let base = board.instantiateInitialViewController()!
				UIApplication.shared.keyWindow?.rootViewController = base
			}
		}
	}
	
	public func transitionToStoryboard<T>(_ board: UIStoryboard) -> (Promise<T>) -> Promise<T> {
		return { p in
			return p.then(on: DispatchQueue.main) { (t: T) -> T in
				let base = board.instantiateInitialViewController()!
				UIApplication.shared.keyWindow?.rootViewController = base
				return t
			}
		}
	}
	
	//MARK: Dismiss
	
	public func transitionDismiss(_ p: Promise<()>) -> Promise<()> {
		return p.then(on: DispatchQueue.main) { [weak self] () -> () in
			let _ = self?.navigationController?.popViewController(animated: true)
		}
	}
	
	public func transitionDismiss<T>(_ p: Promise<T>) -> Promise<T> {
		return p.then(on: DispatchQueue.main) { [weak self] (t: T) -> T in
			let _ = self?.navigationController?.popViewController(animated: true)
			return t
		}
	}
}
