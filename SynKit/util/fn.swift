//
//  fn.swift
//  SynKit
//
//  Created by Maximilian Felgenhauer on 4/7/16.
//  Copyright © 2016 Maximilian Felgenhauer. All rights reserved.
//

import Foundation

func cons<T>(_ head: T, _ tail: [T]) -> [T] {
	var result = tail
	result.insert(head, at: 0)
	return result
}

extension Dictionary {
	func select(_ keys: [Key]) -> [Key: Value] {
		var result = [Key: Value]()
		
		for key in keys {
			result[key] = self[key]
		}
		
		return result
	}
}

func + (url: URL, path: String) -> URL {
	return url.appendingPathComponent(path)
}

func + (url: URL, id: Int64) -> URL {
	return url.appendingPathComponent("\(id)")
}
