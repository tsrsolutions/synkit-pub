//
//  SubjectDataTransaction.swift
//  SynKit
//
//  Created by Maximilian Felgenhauer on 4/7/16.
//  Copyright © 2016 Maximilian Felgenhauer. All rights reserved.
//

import CoreData
import PromiseKit

open class SubjectDataTransaction<T: ModelType>: DataTransaction {
	open let subject: T
	
	init(subject: T, context: NSManagedObjectContext) {
		self.subject = subject
		super.init(context: context)
	}
	
	
	open func mutateSubject(_ fn: @escaping (T) throws -> ()) {
		run {
			do {
				try fn(self.subject)
			} catch let err {
				self.err = err
			}
		}
	}
	
	open func mutateSubject<O>(_ fn: @escaping (T, NSManagedObjectContext) throws -> Promise<O>) -> Promise<O> {
		return Promise { resolve, reject in
			self.run {
				do {
					let prom = try fn(self.subject, self.context)
					let _ = prom.then { (x: O) -> Void in
						resolve(x)
					}
					prom.catch { err in
						reject(err)
					}
				} catch let err {
					reject(err)
				}
			}
		}
	}
	
	open var promise: Promise<T> {
		return Promise { resolve, reject in
			operation = operation.then { _ -> Void in
				if let err = self.err {
					reject(err)
				} else {
					resolve(self.subject)
				}
			}
		}
	}
}

public extension SubjectDataTransaction where T: Syncable {
	public func sync() -> Promise<T> {
		return Promise { resolve, reject in
			run {
				return self.subject.sync(nil, inContext: self.context).then { subject in
					
					resolve(subject)
					}
					
					.catch { err in
						reject(err)
				}
				
			}
		}
	}
	
	public func commit() -> Promise<T> {
		return self.sync().then { _ in
			return self.endTransaction()
		}
	}
}

public extension SubjectDataTransaction {
	public func mutateSubject(_ fn: @escaping (T, Adoption) throws -> ()) {
		run {
			do {
				try fn(self.subject, Adoption(context: self.context))
			} catch let err {
				self.err = err
			}
		}
	}
	
	
	public func endTransaction() -> Promise<T> {
		return Promise { resolve, reject in
			run {
				
				do {
					try self.context.save()
					do {
						try self.context.parent?.save()
						resolve(self.subject)
					} catch let err {
						self.err = err
						self.context.rollback()
						reject(err)
					}
				} catch let err {
					self.err = err
					self.context.rollback()
					reject(err)
				}
			}
		}
	}
}

