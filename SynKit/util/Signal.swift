//
//  Signal.swift
//  SynKit
//
//  Created by Maximilian Felgenhauer on 5/16/16.
//  Copyright © 2016 Maximilian Felgenhauer. All rights reserved.
//

import Foundation

open class Signal<Value> {
	open var value: Value {
		didSet {
			for sub in subscribers {
				sub.apply(value)
			}
		}
	}
	fileprivate var subscribers = [Observer<Value>]()
	
	public init(_ value: Value) {
		self.value = value
	}
	
	open func subscribe(_ observer: Observer<Value>) {
		subscribers.append(observer)
	}
	
	open func unsubscribe(_ observer: Observer<Value>) {
		subscribers.remove(observer)
	}
}

open class Observer<T>: NSObject {
	let apply: (T) -> ()
	
	public init(_ fn : @escaping (T) -> ()) {
		self.apply = fn
	}
}

private extension Array where Element: NSObject {
	mutating func remove(_ element: Element) {
		var mfd: Int?
		for (index, e) in enumerated() {
			if e === element {
				mfd = index
			}
		}
		
		if let removeIndex = mfd {
			self.remove(at: removeIndex)
		}
	}
}


public func lift<A, B, C>(_ fn: @escaping (A, B) -> C) -> (Signal<A>, Signal<B>) -> Signal<C> {
	return { x, y in
		let result = Signal(fn(x.value, y.value))
		
		x.subscribe(Observer { a in result.value = fn(a, y.value) })
		y.subscribe(Observer { b in result.value = fn(x.value, b) })
		
		return result
	}
}


public func lift<A, B>(_ fn: @escaping (A) -> B) -> (Signal<A>) -> Signal<B> {
	return { x in
		let result = Signal(fn(x.value))
		
		x.subscribe(Observer { a in result.value = fn(a) })
		
		return result
	}
}

extension Array where Element: Signal<Bool> {
	public var and: Signal<Bool> {
		let and = { (a: Bool, b: Bool) -> Bool in a && b }
		return self.reduce(Signal(true), lift(and))
	}
}


public func <<<T>(des: Signal<T>, src: Signal<T>) -> Observer<T> {
	let obs = Observer { x in des.value = x }
	src.subscribe(obs)
	return obs
}

