//
//  Listener.swift
//  SynKit
//
//  Created by Maximilian Felgenhauer on 4/7/16.
//  Copyright © 2016 Maximilian Felgenhauer. All rights reserved.
//

import Foundation
import CoreData

open class Listener: NSObject {
	fileprivate let subject: NSManagedObject
	fileprivate let action: (NSManagedObject) -> ()
	
	init(subject: NSManagedObject, action: @escaping (NSManagedObject) -> ()) {
		self.subject = subject
		self.action = action
	}
	
	@objc func listen(_ note: Notification) {
		guard let objs = (note as NSNotification).userInfo?[NSUpdatedObjectsKey] else {
			return
		}
		
		let set = objs as! Set<NSManagedObject>
		
		guard set.contains(subject) else {
			return
		}
		
		action(subject)
	}
}


private var horen = [NSManagedObject:[Listener]]()
private let horenLock = NSLock()

public extension NSManagedObjectContext {
	public func listenTo(_ obj: NSManagedObject, action: @escaping (NSManagedObject) -> Void) -> Listener {
		horenLock.lock()
		defer { horenLock.unlock() }
		
		guard !horen.keys.contains(obj) else {
			fatalError("Only one observer is allowed on one object at a time")
		}
		
		let listener = Listener(subject: obj, action: action)
		horen[obj] = cons(listener, horen[obj] ?? [])
		
		NotificationCenter.default.addObserver(listener, selector: #selector(Listener.listen(_:)), name: NSNotification.Name.NSManagedObjectContextObjectsDidChange, object: self)
		return listener
	}
	
	fileprivate static let heyListen = #selector(Listener.listen(_:))
	
	public func ignore(_ obj: NSManagedObject) {
		horenLock.lock()
		defer { horenLock.unlock() }
		
		guard let listener = horen.removeValue(forKey: obj) else {
			return
		}
		
		NotificationCenter.default.removeObserver(listener)
	}
	
	public func ignore(token: Listener) {
		horenLock.lock()
		defer { horenLock.unlock() }
		
		let elements = horen[token.subject] ?? []
		horen[token.subject] = elements.filter { $0 != token }
		
		if elements.contains(token) {
			NotificationCenter.default.removeObserver(token)
		}
	}
}
