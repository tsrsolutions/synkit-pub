//
//  Adoption.swift
//  SynKit
//
//  Created by Maximilian Felgenhauer on 4/7/16.
//  Copyright © 2016 Maximilian Felgenhauer. All rights reserved.
//

import Foundation
import CoreData

public extension NSManagedObjectContext {
	public func adopt<T: NSManagedObject>(_ obj: T) -> T? {
		guard obj.managedObjectContext != self else {
			return obj
		}
		return self.object(with: obj.objectID) as? T
	}
}

public struct Adoption {
	fileprivate let context: NSManagedObjectContext
	init(context: NSManagedObjectContext) {
		self.context = context
	}
	
	public func adopt<T: ModelType>(_ obj: T) -> T? where T: NSManagedObject{
		return context.adopt(obj)
	}
}
