//
//  Fetchable.swift
//  SynKit
//
//  Created by Maximilian Felgenhauer on 4/7/16.
//  Copyright © 2016 Maximilian Felgenhauer. All rights reserved.
//

import CoreData
import PromiseKit

public protocol Fetchable: ModelType, RoutableCollection, Inflatable, HasCollectionName {
	/**
	
	Attempts to fetch all of this type it can.
	
	- Parameter context: an database context in which to inflate a model
	- Parameter parameters: parameters to pass to the web sever to provide further context
	- Returns: A Promise of the models that are parsed.
	- Remark: This method should use the HTTP method GET on some variation of the routeCollection of the model
	*/
	static func fetch(inContext context: NSManagedObjectContext, withParameters parameters: [String:String]) -> Promise<[Self]>
}
