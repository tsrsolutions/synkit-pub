//
//  SynKit.h
//  SynKit
//
//  Created by Maximilian Felgenhauer on 4/7/16.
//  Copyright © 2016 Maximilian Felgenhauer. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for SynKit.
FOUNDATION_EXPORT double SynKitVersionNumber;

//! Project version string for SynKit.
FOUNDATION_EXPORT const unsigned char SynKitVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <SynKit/PublicHeader.h>


