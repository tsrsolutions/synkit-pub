//
//  FlexCollectionViewLayout.swift
//  SynKit
//
//  Created by Maximilian Felgenhauer on 5/17/16.
//  Copyright © 2016 Maximilian Felgenhauer. All rights reserved.
//

import Foundation
import UIKit

public enum FlexLayoutType {
	case center
	case spaceAround
	case spaceBetween
	case left
	case right
}

open class FlexCollectionViewLayout: UICollectionViewFlowLayout {
	fileprivate let cache = Cache<IndexPath, UICollectionViewLayoutAttributes>()
	
	fileprivate let xAligner: XAligner
	open let type: FlexLayoutType
	
	public init(type: FlexLayoutType) {
		self.type = type
		self.xAligner = type.aligner
		super.init()
	}
	
	public required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	open override func prepare() {
		cache.clear()
	}
	
	open override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
		guard let view = collectionView else {
			fatalError()
		}
		let pathes = (0..<view.numberOfSections).flatMap { sec in
			(0..<view.numberOfItems(inSection: sec)).map { cell in
				IndexPath(item: cell, section: sec)
			}
		}
		
		return pathes
			.map { path in layoutAttributesForItem(at: path)! }
			.filter { attr in attr.frame.intersects(rect) }
	}
	
	open override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
		return cache.get(indexPath) { path in
			let attr = super.layoutAttributesForItem(at: indexPath)!
			let rowAttrs = sectionLayoutAttributes((indexPath as NSIndexPath).section).filter { potential in
				alignsY(attr.frame, b: potential.frame)
			}
			
			let x = xAligner.align(attr.frame, withRects: rowAttrs.map { $0.frame }, withinBound: collectionView!.frame, withSpace: minimumInteritemSpacing)
			let frame = CGRect(x: x, y: attr.frame.minY, width: attr.frame.width, height: attr.frame.height)
			let result = attr.copy() as! UICollectionViewLayoutAttributes
			result.frame = frame
			return result
		}
	}
	
	fileprivate func sectionLayoutAttributes(_ section: Int) -> [UICollectionViewLayoutAttributes] {
		guard let view = collectionView else {
			fatalError()
		}
		
		return (0..<view.numberOfItems(inSection: section)).map { item in
			super.layoutAttributesForItem(at: IndexPath(item: item, section: section))!
		}
	}
	
	open override func layoutAttributesForSupplementaryView(ofKind elementKind: String, at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
		return super.layoutAttributesForSupplementaryView(ofKind: elementKind, at: indexPath)
	}
}


private func sum<T: Sequence>(_ li: T) -> CGFloat where T.Iterator.Element == CGFloat {
	return li.reduce(0, +)
}

private func alignsY(_ a: CGRect, b: CGRect) -> Bool {
	let max = a.minY <= b.maxY && b.maxY <= a.maxY
	let min = a.minY <= b.minY && b.minY <= a.maxY
	return min || max
}

private protocol XAligner {
	func align(_ orig: CGRect, withRects: [CGRect], withinBound: CGRect, withSpace: CGFloat) -> CGFloat
}



private extension FlexLayoutType {
	var aligner: XAligner {
		get {
			switch self {
			case .center:
				return CenterXAligner()
			case .left:
				return LeftXAligner()
			case .right:
				return RightXAligner()
			case .spaceAround:
				return SpaceAroundXAligner()
			case .spaceBetween:
				return SpaceInBetweenXAligner()
			}
		}
	}
}

private class CenterXAligner: XAligner {
	func align(_ orig: CGRect, withRects rects: [CGRect], withinBound bound: CGRect, withSpace space: CGFloat) -> CGFloat {
		
		let space = CGFloat(rects.count - 1) * space
		let taken = sum(rects.map { $0.width })
		let offset = (bound.width - (taken + space)) / 2
		
		guard let index = rects.index(of: orig) else {
			fatalError()
		}
		
		return sum(rects[0..<index].map { $0.width }) + (space * CGFloat(index)) + offset
	}
}

private class RightXAligner: XAligner {
	func align(_ orig: CGRect, withRects rects: [CGRect], withinBound bound: CGRect, withSpace space: CGFloat) -> CGFloat {
		guard let index = rects.index(of: orig) else {
			fatalError()
		}
		
		let taken = sum(rects[index..<rects.count].map { $0.width })
		let blank = CGFloat(rects.count) - (CGFloat(index) * space)
		
		return bound.width - (taken + blank)
	}
}

private class LeftXAligner: XAligner {
	fileprivate func align(_ orig: CGRect, withRects rects: [CGRect], withinBound bounds: CGRect, withSpace space: CGFloat) -> CGFloat {
		guard let index = rects.index(of: orig) else {
			fatalError()
		}
		
		return sum(rects[0..<index].map { $0.width }) + space * CGFloat(index)
	}
}

private class SpaceInBetweenXAligner: XAligner {
	fileprivate func align(_ orig: CGRect, withRects rects: [CGRect], withinBound bound: CGRect, withSpace space: CGFloat) -> CGFloat {
		guard let index = rects.index(of: orig) else {
			fatalError()
		}
		
		let totalSpace = bound.width - sum(rects.map { $0.width })
		
		let space = totalSpace / CGFloat(rects.count)
		
		return sum(rects[0..<index].map { $0.width }) + (space * CGFloat(index))
	}
}

private class SpaceAroundXAligner: XAligner {
	fileprivate func align(_ orig: CGRect, withRects rects: [CGRect], withinBound bound: CGRect, withSpace space: CGFloat) -> CGFloat {
		guard let index = rects.index(of: orig) else {
			fatalError()
		}
		
		let totalSpace = bound.width - sum(rects.map { $0.width })
		
		let space = totalSpace / CGFloat(rects.count+1)
		
		return sum(rects[0..<index].map { $0.width }) + (space * CGFloat(index+1))
	}
}
