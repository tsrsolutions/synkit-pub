//
//  File.swift
//  SynKit
//
//  Created by Maximilian Felgenhauer on 4/13/16.
//  Copyright © 2016 Maximilian Felgenhauer. All rights reserved.
//

import Foundation

infix operator |>: AdditionPrecedence // { associativity left precedence 100 }
infix operator >>>: MultiplicationPrecedence //{ associativity left precedence 120 }

// Function Application

public func |> <A, B>(lhs: A, rhs: (A) -> B) -> B {
	return rhs(lhs)
}

// Function Composition

public func >>> <A, B, C>(lhs: @escaping (A)->B, rhs: @escaping (B)->C) -> (A) -> C {
	return { a in
		return rhs(lhs(a))
	}
}

