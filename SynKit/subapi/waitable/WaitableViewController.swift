//
//  WaitableViewController.swift
//  SynKit
//
//  Created by Maximilian Felgenhauer on 4/13/16.
//  Copyright © 2016 Maximilian Felgenhauer. All rights reserved.
//

import Foundation

import UIKit
import PromiseKit

public protocol WaitableViewController: class {
	var waiting: Bool { get }
	func wait()
	func unwait()
}

private var WaitCount = "tsral.wait-count"
private var WaitIndicator = "tsral.indicator-view"
private let waitLock = NSLock()

public extension WaitableViewController where Self: UIViewController {
	
	fileprivate var _wait_count: Int {
		get {
			return objc_getAssociatedObject(self, &WaitCount) as? Int ?? 0
		}
		
		set {
			guard newValue >= 0 else {
				fatalError("Unbalanced wait calls")
			}
			
			objc_setAssociatedObject(self, &WaitCount, newValue as NSNumber, .OBJC_ASSOCIATION_RETAIN)
		}
	}
	
	public var waiting: Bool {
		return _wait_count != 0
	}
	
	public func wait() {
		waitLock.lock()
		defer { waitLock.unlock() }
		
		UIApplication.shared.resignFirstResponder()
		
		_wait_count += 1
		
		if _wait_count == 1 {
			showIndicator()
		}
	}
	
	public func unwait() {
		waitLock.lock()
		defer { waitLock.unlock() }
		
		_wait_count -= 1
		
		if _wait_count == 0 {
			hideIndicator()
		}
	}
	
	public func waitFor<T>(_ p: Promise<T>) -> Promise<T> {
		wait()
		return p.always { [weak self] in
			self?.unwait()
		}
	}
	
	fileprivate func showIndicator() {
		view.insertSubview(indicator, at: view.subviews.count - 1)
		view.fullview(indicator)
		indicator.start()
	}
	
	fileprivate func hideIndicator() {
		indicator.stop()
		indicator.removeFromSuperview()
	}
	
	
	fileprivate var indicator: BlockingActivityIndicatorView {
		get {
			let view: BlockingActivityIndicatorView
			if let current = objc_getAssociatedObject(self, &WaitIndicator) as? BlockingActivityIndicatorView {
				view = current
			} else {
				view = BlockingActivityIndicatorView()
				objc_setAssociatedObject(self, &WaitIndicator, view, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
			}
			
			return view
		}
	}
}

private class BlockingActivityIndicatorView: UIView {
	let indicator = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		
		translatesAutoresizingMaskIntoConstraints = false
		indicator.translatesAutoresizingMaskIntoConstraints = false
		
		backgroundColor = UIColor(colorLiteralRed: 0, green: 0, blue: 0, alpha: 0.4)
		
		loadSubviews()
		setupConstraints()
	}
	
	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	func loadSubviews() {
		addSubview(indicator)
	}
	
	func setupConstraints() {
		indicator.addConstraints([
			NSLayoutConstraint(item: indicator, attribute: .width, const: 120),
			NSLayoutConstraint(item: indicator, attribute: .height, const: 120)
			])
		
		addConstraints([
			NSLayoutConstraint(item: self, toItem: indicator, onAttribute: .centerX),
			NSLayoutConstraint(item: self, toItem: indicator, onAttribute: .centerY)
			])
	}
	
	func start() {
		indicator.startAnimating()
	}
	
	func stop() {
		indicator.stopAnimating()
	}
}

extension UIView {
	func fullview(_ subview: UIView) {
		let top = NSLayoutConstraint(item: self, toItem: subview, onAttribute: .top)
		let bottom = NSLayoutConstraint(item: self, toItem: subview, onAttribute: .bottom)
		let leading = NSLayoutConstraint(item: self, toItem: subview, onAttribute: .leading)
		let trailing = NSLayoutConstraint(item: self, toItem: subview, onAttribute: .trailing)
		
		self.addConstraints([top, bottom, leading, trailing])
	}
}

extension NSLayoutConstraint {
	convenience init(item: AnyObject, toItem other: AnyObject, onAttribute attribute: NSLayoutAttribute) {
		self.init(item: item, attribute: attribute, relatedBy: .equal, toItem: other, attribute: attribute, multiplier: 1, constant: 0)
	}
	
	convenience init(item: AnyObject, attribute: NSLayoutAttribute, const: CGFloat) {
		self.init(item: item, attribute: attribute, relatedBy: .equal, toItem:  nil, attribute: .notAnAttribute, multiplier: 1, constant: const)
	}
}

