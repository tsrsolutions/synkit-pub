//
//  requests.swift
//  SynKit
//
//  Created by Maximilian Felgenhauer on 9/30/16.
//  Copyright © 2016 Maximilian Felgenhauer. All rights reserved.
//

import Foundation

public enum Method {
	case get
	case post
	case put
	case delete
	case other(String)
	
	var method: String {
		switch self {
		case .get:
			return "GET"
		case .post:
			return "POST"
		case .put:
			return "PUT"
		case .delete:
			return "DELETE"
		case let .other(str):
			return str
		}
	}
}

public struct HTTPOptions {
	public let cachingPolicy: NSURLRequest.CachePolicy
	public let timeout: TimeInterval
	
	public init(cachingPolicy: NSURLRequest.CachePolicy = .useProtocolCachePolicy, timeout: UInt = 30) {
		self.cachingPolicy = cachingPolicy
		self.timeout = TimeInterval(timeout)
	}
	
	public static var `default`: HTTPOptions {
		return HTTPOptions()
	}
	
	func merge(options: HTTPOptions) -> HTTPOptions {
		let cachePolicy = self.cachingPolicy == .useProtocolCachePolicy ? options.cachingPolicy : self.cachingPolicy
		let timeout = self.timeout == 30 ? options.timeout : self.timeout
		
		return HTTPOptions(cachingPolicy: cachePolicy, timeout: UInt(timeout.rounded(.down)))
	}
}

public struct Request {
	public let url: URL
	public let data: Data?
	public let headers: [String: String]
	public let method: Method
	public let options: HTTPOptions
	
	public init(method: Method = .get, url: URL, data: Data? = nil, headers: [String: String] = [:], options: HTTPOptions = .default) {
		self.url = url
		self.method = method
		self.data = data
		self.headers = headers
		self.options = options
	}
}

extension URLSession {
	public typealias Handler = (Data?, HTTPError?) -> Void
	
	@discardableResult
	public func send(_ request: Request, passing expectations: [HTTPExpectation] = [], with options: HTTPOptions = .default, then: @escaping Handler) -> Cancellable {
		let req = generateRequest(request: request, options: options)
		
		let task = self.dataTask(with: req) { (data, res, err) in
			guard err == nil else {
				then(data, WrappedHTTPError(reason: err!))
				return
			}
			
			guard let response = res as? HTTPURLResponse else {
				fatalError("This api should only be used for http")
			}
			
			for expectation in expectations {
				do {
					try expectation.expect(data: data, response: response)
				} catch let err as HTTPError {
					then(data, err)
					return
				} catch {
					fatalError("Invalid error thrown from expectation \(expectation)")
				}
			}
			
			then(data, nil)
		}
		
		task.resume()
		
		return task
	}
}


func generateRequest(request: Request, options: HTTPOptions) -> URLRequest {
	let opts = options.merge(options: request.options)
	let req = NSMutableURLRequest(url: request.url, cachePolicy: opts.cachingPolicy, timeoutInterval: opts.timeout)
	
	req.httpMethod = request.method.method
	req.httpBody = request.data
	
	for (key, value) in request.headers {
		req.addValue(value, forHTTPHeaderField: key)
	}
	
	if let data = request.data {
		req.addValue(String(format: "%u", data.count), forHTTPHeaderField: "Content-Length")
	}
	
	return req as URLRequest
}

public protocol Cancellable { func cancel() }
extension URLSessionDataTask: Cancellable { }

