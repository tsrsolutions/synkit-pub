//
//  Cache.swift
//  SynKit
//
//  Created by Maximilian Felgenhauer on 5/17/16.
//  Copyright © 2016 Maximilian Felgenhauer. All rights reserved.
//

import Foundation

open class Cache<K: Hashable, V> {
	fileprivate var cache: [K:V] = [:]
	
	open func get(_ key: K, fn: (K) -> V) -> V {
		if let value = cache[key] {
			return value
		}
	
		let value = fn(key)
		cache[key] = value
		
		return value
	}
	
	open func clear() {
		cache = [:]
	}
}
