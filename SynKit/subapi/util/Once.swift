//
//  Once.swift
//  SynKit
//
//  Created by Maximilian Felgenhauer on 4/14/16.
//  Copyright © 2016 Maximilian Felgenhauer. All rights reserved.
//

import Foundation

public func once(_ fn: @escaping () -> ()) -> () -> () {
	var called = false
	
	return {
		guard !called else {
			return
		}
		
		called = true
		fn()
	}
}

public func once<T>(_ fn: @escaping (T) -> ()) -> (T) -> () {
	var called = false
	
	return { x in
		guard !called else {
			return
		}
		
		called = true
		fn(x)
	}
}
