//
//  Keychain.swift
//  SynKit
//
//  Created by Maximilian Felgenhauer on 4/13/16.
//  Copyright © 2016 Maximilian Felgenhauer. All rights reserved.
//

import Foundation
import Security

private let SecClass = kSecClass as String
private let SecAttrService = kSecAttrService as String
private let SecAttrAccount = kSecAttrAccount as String
private let SecValueData = kSecValueData as String
private let SecClassInternetPassword = kSecClassInternetPassword as String
private let SecClassGenericPassword = kSecClassGenericPassword as String
private let SecMatchLimitOne = kSecMatchLimitOne as String
private let SecMatchLimit = kSecMatchLimit as String
private let SecReturnData = kSecReturnData as String

open class KeyChain {
	let serviceId: String
	public init(serviceId: String) {
		self.serviceId = serviceId
	}
	
	open func savePassword(_ password: String, forUsername username: String) throws {
		
		let encoded = password.data(using: String.Encoding.utf8, allowLossyConversion: false)!
		
		let query = [
			SecClass: SecClassGenericPassword,
			SecAttrAccount: username,
			SecAttrService: serviceId,
			SecValueData: encoded
		] as [String : Any]
		
		let deleteStatus = SecItemDelete(query as CFDictionary)
		guard deleteStatus == errSecSuccess || deleteStatus == errSecItemNotFound else {
			print("Delete op error")
			throw asSecurityError(deleteStatus)
		}
		
		let addStatus = SecItemAdd(query as CFDictionary, nil)
		guard addStatus == errSecSuccess else {
			print("Add op error")
			throw asSecurityError(addStatus)
		}
	}
	
	open func fetchPasswordForUsername(_ username: String) throws -> String? {
		let query = [
			SecClass: SecClassGenericPassword,
			SecAttrAccount: username,
			SecAttrService: serviceId,
			SecMatchLimit: SecMatchLimitOne,
			SecReturnData: true
		] as [String : Any]
		
		let dataRef: UnsafeMutablePointer<AnyObject?> = UnsafeMutablePointer<AnyObject?>.allocate(capacity: 1)
		dataRef.initialize(to: nil)
		defer {
			dataRef.deinitialize()
			dataRef.deallocate(capacity: 1)
		}
		
		let status = SecItemCopyMatching(query as CFDictionary, dataRef)
		guard status == errSecSuccess else {
			throw asSecurityError(status)
		}
		
		guard let data = dataRef.pointee as? Data else {
			return nil
		}
		
		return String(data: data, encoding: String.Encoding.utf8)
	}
	
	open func deletePasswordForUsername(_ username: String) throws {
		let query = [
			SecClass: SecClassGenericPassword,
			SecAttrAccount: username,
			SecAttrService: serviceId
		]
		
		let deleteStatus = SecItemDelete(query as CFDictionary)
		guard deleteStatus == errSecSuccess else {
			throw asSecurityError(deleteStatus)
		}
	}
	
}

public enum KeyChainError: Error {
	case unimplemented
	case parameter
	case allocation
	case notAvailable
	case authFailed
	case duplicateItem
	case notFound
	case interactionNotAllowed
	case decode
	case unknown(OSStatus)
}

//This code is kind of horrible, but more robost then simpler methods
private func asSecurityError(_ status: OSStatus) -> KeyChainError {
	switch status {
	case errSecUnimplemented:
		return .unimplemented
	case errSecAllocate:
		return .allocation
	case errSecParam:
		return .parameter
	case errSecNotAvailable:
		return .notAvailable
	case errSecAuthFailed:
		return .authFailed
	case errSecDuplicateItem:
		return .duplicateItem
	case errSecItemNotFound:
		return .notFound
	case errSecInteractionNotAllowed:
		return .interactionNotAllowed
	case errSecDecode:
		return .decode
	default:
		return .unknown(status)
	}
}
