//
//  JQKit.swift
//  SynKit
//
//  Created by Maximilian Felgenhauer on 9/19/16.
//  Copyright © 2016 Maximilian Felgenhauer. All rights reserved.
//

import Foundation

public func query(json: Any) -> JSONQuery {
	return JSONQuery(json: json)
}

public func query(data: Data) throws -> JSONQuery {
	return JSONQuery(json: try JSONSerialization.jsonObject(with: data))
}

public struct JSONQuery {
	private let reference: Any
	
	fileprivate init(json: Any) {
		self.reference = json
	}
	
	var root: Any {
		return reference
	}
	
	private func mustBe<T>(ofType type: T.Type) throws -> T {
		guard let ref = reference as? T else {
			throw JQError.typeMismatch(actual: type(of: reference), expected: T.self)
		}
		
		return ref
	}
	
	public func string() throws -> String {
		return try mustBe(ofType: String.self)
	}
	
	public func bool() throws -> Bool {
		return try mustBe(ofType: Bool.self)
	}
	
	public func integer() throws -> Int {
		return try mustBe(ofType: Int.self)
	}
	
	public func float() throws -> Float {
		return try mustBe(ofType: Float.self)
	}
	
	public func double() throws -> Double {
		return try mustBe(ofType: Double.self)
	}
	
	public func array() throws -> [Any] {
		return try mustBe(ofType: [Any].self)
	}
	
	public func array<T>(ofType type: T.Type) throws -> [T] {
		return try mustBe(ofType: [T].self)
	}
	
	public func object() throws -> [String:Any] {
		return try mustBe(ofType: [String: Any].self)
	}
}


public enum JQError: Error {
	case typeMismatch(actual: Any.Type, expected: Any.Type)
	case missingKey(key: String, object: [String: Any])
	case outOfIndex(index: Int, array: [Any])
}

extension JQError: Equatable {
	public static func ==(lhs: JQError, rhs: JQError) -> Bool {
		switch (lhs, rhs) {
		case let (.typeMismatch(actual: la, expected: le), .typeMismatch(actual: ra, expected: re)):
			return la == ra && le == re
		case let (.missingKey(key: lk, object: _), .missingKey(key: rk, object: _)):
			return lk == rk
		case let (.outOfIndex(index: li, array: _), .outOfIndex(index: ri, array: _)):
			return li == ri
			
		default:
			return false
		}
	}
}


public protocol JQKey { }

extension String: JQKey { }
extension Int: JQKey { }


extension JSONQuery {
	public func through(keys keyPath: JQKey...) throws -> JSONQuery {
		var current = root
		
		for key in keyPath {
			switch (key, current) {
			case let (key as String, val as [String: Any]):
				if let next = val[key] {
					current = next
				} else {
					throw JQError.missingKey(key: key, object: val)
				}
				
			case let (key as Int, val as [Any]):
				if key < val.count {
					current = val[key]
				} else {
					throw JQError.outOfIndex(index: key, array: val)
				}
				
			default:
				switch key {
				case is Int:
					throw JQError.typeMismatch(actual: type(of: current), expected: [Any].self)
				case is String:
					throw JQError.typeMismatch(actual: type(of: current), expected: [String:Any].self)
					
				default:
					fatalError("Switch should be exhaustive")
				}
			}
		}
		
		return JSONQuery(json: current)
	}
}
