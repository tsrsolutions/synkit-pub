//
//  Decodable.swift
//  SynKit
//
//  Created by Maximilian Felgenhauer on 9/19/16.
//  Copyright © 2016 Maximilian Felgenhauer. All rights reserved.
//

import Foundation


public protocol Decodable {
	init(json: Any) throws
}

extension JSONQuery {
	public func decodable<T: Decodable>(_ type: T.Type) throws -> T {
		return try T(json: root)
	}
	
	public func decodables<T: Decodable>(_ type: T.Type) throws -> [T] {
		guard let values = root as? [Any] else {
			throw JQError.typeMismatch(actual: type(of: root), expected: [Any].self)
		}
		
		return try values.map { try T(json: $0) }
	}
}

