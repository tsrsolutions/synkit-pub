//
//  ContactStore.swift
//  Guardian
//
//  Created by Maximilian Felgenhauer on 1/5/16.
//  Copyright © 2016 Maximilian Felgenhauer. All rights reserved.
//

import Foundation
import Contacts

public struct ContactKey: OptionSet {
	public let rawValue: Int
	
	public init(rawValue: Int) {
		self.rawValue = rawValue
	}
	
	public static let GivenName = ContactKey(rawValue: 1 << 0)
	public static let FamilyName = ContactKey(rawValue: 1 << 1)
	public static let ImageData = ContactKey(rawValue: 1 << 2)
	public static let ImageDataAvailable = ContactKey(rawValue: 1 << 3)
	public static let JobTitle = ContactKey(rawValue: 1 << 4)
	public static let OrganizationName = ContactKey(rawValue: 1 << 5)
	public static let EmailAddresses = ContactKey(rawValue: 1 << 6)
	public static let PhoneNumbers = ContactKey(rawValue: 1 << 7)
	
	public static let Names: ContactKey = [ContactKey.GivenName, ContactKey.FamilyName]
	public static let Image: ContactKey = [ContactKey.ImageData, ContactKey.ImageDataAvailable]
	public static let Job: ContactKey = [ContactKey.JobTitle, ContactKey.OrganizationName, ContactKey.EmailAddresses]
	
	public static let All: ContactKey = [ContactKey.Names, ContactKey.Image, ContactKey.Job, ContactKey.PhoneNumbers]
	
	var keys: [String] {
		var keys: [String] = []
		
		for i in (0..<ContactKey.contactKeys.count) {
			if (rawValue & (1 << i)) != 0 {
				keys.append(ContactKey.contactKeys[i])
			}
		}
		
		return keys
	}
	
	
	fileprivate static let contactKeys = [
		CNContactGivenNameKey,
		CNContactFamilyNameKey,
		CNContactImageDataKey,
		CNContactImageDataAvailableKey,
		CNContactJobTitleKey,
		CNContactOrganizationNameKey,
		CNContactEmailAddressesKey,
		CNContactPhoneNumbersKey
	]
}

private let store = CNContactStore()

public func fetchContacts(_ keys: ContactKey, withPredicate predicate: NSPredicate? = nil) throws -> [CNContact] {
	let request = CNContactFetchRequest(keysToFetch: keys.keys as [CNKeyDescriptor])
	request.predicate = predicate
	return try store.executeFetchRequest(request)
}


extension CNContactStore {
	func executeFetchRequest(_ fetchRequest: CNContactFetchRequest) throws -> [CNContact] {
		var contacts: [CNContact] = []
		
		do {
			try enumerateContacts(with: fetchRequest) { contact, _ in
				contacts.append(contact)
			}
		} catch let err {
			throw err
		}
		
		return contacts
	}
}

public extension CNContact {
	public var emails: [String] {
		return emailAddresses.map { namedValue in
			namedValue.value as String
		}
	}
	
	public var mobilePhone: String? {
		for acceptableLabel in CNContact.mobileLabels {
			for field in phoneNumbers {
				if field.label == acceptableLabel {
					return field.value.stringValue
				}
			}
		}
		
		return nil
	}
	
	public var directPhone: String? {
		for acceptableLabel in CNContact.mobileLabels {
			for field in phoneNumbers {
				if field.label == acceptableLabel {
					return field.value.stringValue
				}
			}
		}
		
		return nil
	}
	
	fileprivate static let directLabels = [CNLabelPhoneNumberMain, CNLabelPhoneNumberWorkFax, CNLabelPhoneNumberPager]
	fileprivate static let mobileLabels = [CNLabelPhoneNumberiPhone, CNLabelPhoneNumberMobile]
}
