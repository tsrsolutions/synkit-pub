//
//  DateField.swift
//  SynKit
//
//  Created by Maximilian Felgenhauer on 5/4/16.
//  Copyright © 2016 Maximilian Felgenhauer. All rights reserved.
//

import Foundation
import UIKit

public protocol DateFieldDelegate: class {
	func dateField(_ dateField: DateField, didSelectDate date: Date)
}

@IBDesignable
open class DateField: UITextField {
	
	open var date: Date? {
		didSet {
			if date == nil {
				text = ""
			} else {
				text = (dateFormatter ?? DateField.defaultDateFormatter)?.string(from: date!)
				dateDelegate?.dateField(self, didSelectDate: date!)
			}
		}
	}
	open weak var dateDelegate: DateFieldDelegate?
	
	@IBInspectable
	open var format: String = "" {
		didSet {
			dateFormatter = DateFormatter(format: format)
		}
	}
	
	@IBInspectable
	open var steps: Int = 1 {
		didSet {
			dateView?.minuteInterval = steps
		}
	}
	
	fileprivate var dateFormatter: DateFormatter?
	fileprivate var dateView: UIDatePicker?
	
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		load()
	}
	
	public required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		load()
	}
	
	func load() {
		dateView = UIDatePicker()
		dateView?.addTarget(self, action: #selector(onDateChange(_:)), for: .valueChanged)
		inputView = dateView
	}
	
	func onDateChange(_ picker: UIDatePicker) {
		date = picker.date
	}
	
	fileprivate static let defaultDateFormatter = DateFormatter(format: "hh:mm:ss")
}

public extension DateFormatter {
	convenience init(format: String) {
		self.init()
		self.dateFormat = format
	}
}
