//
//  TFFocus.swift
//  SynKit
//
//  Created by Maximilian Felgenhauer on 4/13/16.
//  Copyright © 2016 Maximilian Felgenhauer. All rights reserved.
//

import Foundation
import UIKit

open class TFFocus: NSObject, UITextFieldDelegate  {
	fileprivate let fields: [UITextField]
	fileprivate let onComplete: () -> ()
	
	public init(fields: [UITextField], onComplete: @escaping () -> ()) {
		self.fields = fields
		self.onComplete = onComplete
		
		super.init()
		
		for field in fields {
			field.delegate = self
		}
	}
	
	open func textFieldShouldReturn(_ textField: UITextField) -> Bool {
		guard let index = fields.index(of: textField) else {
			fatalError("TFFocus should only have text fields inside the focus use it as a delegate")
		}
		
		guard index != fields.count - 1 else {
			onComplete()
			return true
		}
		
		textField.resignFirstResponder()
		fields[index+1].becomeFirstResponder()
		return true
	}
}
