//
//  Syncable.swift
//  SynKit
//
//  Created by Maximilian Felgenhauer on 4/7/16.
//  Copyright © 2016 Maximilian Felgenhauer. All rights reserved.
//

import Foundation
import CoreData
import PromiseKit

public protocol Syncable: class, ModelType, Inflatable, Encodable, RoutableEntity, RoutableCollection {
	/**
	Attempts to sync this object with the remote server.
	
	- Parameter fields: subsets the syncing attributes if present
	- Paramater context: a context to inflate the servers compermise
	- Returns: Itself after being processed by the server
	- Remark: This method could use PUT on the entities route if the enitity is on the web server, otherwise it will use POST on the collections route.
	*/
	func sync(_ fields: [String]?, inContext context: NSManagedObjectContext) -> Promise<Self>
}
