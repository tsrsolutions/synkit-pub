//
//  Parsable.swift
//  SynKit
//
//  Created by Maximilian Felgenhauer on 4/7/16.
//  Copyright © 2016 Maximilian Felgenhauer. All rights reserved.
//

import CoreData
import PromiseKit

/**
Describes a type that can take on the representation of a different type
*/
public protocol Inflatable {
	/**
	
	- parameter rep: a representation to take on
	- Postcondition: self appears like Representation
	*/
	func inflate(_ rep: Any) throws
}


public protocol HasCollectionName {
	static var collectionName: String { get }
}

public protocol Processable {
	static func processMany(_ context: NSManagedObjectContext) -> (Any) -> Promise<Array<Self>>
}
