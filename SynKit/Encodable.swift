//
//  Encodable.swift
//  SynKit
//
//  Created by Maximilian Felgenhauer on 4/7/16.
//  Copyright © 2016 Maximilian Felgenhauer. All rights reserved.
//

/**
Models can encode it attributes
*/
public protocol Encodable {
	
	/**
	- Returns: Attributes used for syncing
	- Postcondition: All values must be primatives i.e, numbers, strings, bools, arrays of primitives, dictionary of primatives.
	*/
	var encoded: [String: AnyObject] { get }
}