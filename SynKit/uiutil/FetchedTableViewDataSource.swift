//
//  FetchedTableViewDataSource.swift
//  SynKit
//
//  Created by Maximilian Felgenhauer on 4/7/16.
//  Copyright © 2016 Maximilian Felgenhauer. All rights reserved.
//

import Foundation
import UIKit
import CoreData

open class FetchedTableViewDataSource<T: NSManagedObject>: NSObject, NSFetchedResultsControllerDelegate, UITableViewDataSource {
	
	fileprivate let resultsController: NSFetchedResultsController<T>
	fileprivate let identifier: String
	fileprivate weak var view: UITableView?
	fileprivate let sectioned: Bool
	fileprivate let indexed: Bool
	fileprivate let customize: Optional<(UITableViewCell) -> ()>
	
	fileprivate var _invariant: NSPredicate?
	open var predicate: NSPredicate? {
		
		didSet {
			let combined = NSCompoundPredicate(andPredicateWithSubpredicates: [_invariant, predicate].flatMap{$0})
			resultsController.fetchRequest.predicate = combined
			
			do {
				try resultsController.performFetch()
				view?.reloadData()
			} catch let err {
				print("found error", err)
			}
		}
	}
	
	public init(request: NSFetchRequest<T>, context: NSManagedObjectContext, section: String? = nil, identifier: String? = nil, indexed: Bool = false, customize: Optional<(UITableViewCell) -> ()> = nil, tableView: UITableView) {
		self.view = tableView
		self.identifier = identifier ?? "Default"
		self.resultsController = NSFetchedResultsController(fetchRequest: request, managedObjectContext: context, sectionNameKeyPath: section, cacheName: nil)
		
		self.sectioned = section != nil
		self.indexed = indexed
		
		self.customize = customize
		
		super.init()
		
		_invariant = request.predicate
		
		self.resultsController.delegate = self
		try! self.resultsController.performFetch()
	}
	
	
	//MARK: Table View Delegate
	
	open func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		guard let obj = resultsController.object(at: indexPath) as? ModelType else {
			fatalError("FetchedTableViewDataSource expected object of type: ModelType")
		}
		
		guard let baseCell = tableView.dequeueReusableCell(withIdentifier: identifier) else {
			fatalError("FetchedTableViewDataSource expected cell for identifier: \(identifier)")
		}
		
		guard let cell = baseCell as? ConfigurableCell else {
			fatalError("Expected cell to be contextualizable with a ModelType")
		}
		
		cell.configure(withModel: obj)
		customize?(baseCell)
		
		return cell as! UITableViewCell
	}
	
	open func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		guard let section = resultsController.sections?[section] else {
			return 0
		}
		
		return section.numberOfObjects
	}
	
	open func numberOfSections(in tableView: UITableView) -> Int {
		guard sectioned else {
			return 1
		}
		
		return resultsController.sectionIndexTitles.count
	}
	
	open func sectionIndexTitles(for tableView: UITableView) -> [String]? {
		guard sectioned && indexed else {
			return nil
		}
		
		return resultsController.sectionIndexTitles
	}
	
	open func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
		guard sectioned else {
			return nil
		}
		
		return resultsController.sectionIndexTitles[section]
	}
	
	//MARK: NSFetchedResultsControllerDelegate
	
	open func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
		self.view?.beginUpdates()
	}
	
	open func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
		self.view?.endUpdates()
	}
	
	open func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
		
		DispatchQueue.main.async { [weak self] in
			switch type {
			case .insert:
				self?.view?.insertRows(at: [newIndexPath!], with: .automatic)
				break
				
			case .update:
				self?.view?.reloadRows(at: [indexPath!], with: .automatic)
				break
				
			case .move:
				self?.view?.reloadRows(at: [indexPath!, newIndexPath!], with: .automatic)
				break
				
			case .delete:
				self?.view?.deleteRows(at: [indexPath!], with: .automatic)
				break
			}
		}
	}
	
	open var selectedObject: ModelType? {
		guard let indexPath = view?.indexPathForSelectedRow else {
			return nil
		}
		
		return (resultsController.object(at: indexPath) as! ModelType)
	}
	
	open func objectForIndexPath(_ indexPath: IndexPath) -> ModelType {
		return resultsController.object(at: indexPath) as! ModelType
	}
}
