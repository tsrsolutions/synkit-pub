//
//  ConfigurableCell.swift
//  SynKit
//
//  Created by Maximilian Felgenhauer on 4/7/16.
//  Copyright © 2016 Maximilian Felgenhauer. All rights reserved.
//

import Foundation

public protocol ConfigurableCell {
	func configure(withModel model:ModelType)
}