//
//  ArrayDataSource.swift
//  SynKit
//
//  Created by Maximilian Felgenhauer on 4/14/16.
//  Copyright © 2016 Maximilian Felgenhauer. All rights reserved.
//

import Foundation

open class ArrayDataSource: NSObject, UITableViewDataSource {
	fileprivate let identifer: String
	fileprivate let content: [AnyObject]
	fileprivate let config: (AnyObject, UITableViewCell) -> Void
	
	public init(identifer: String, content: [AnyObject], config: @escaping (AnyObject, UITableViewCell) -> Void ) {
		self.identifer = identifer
		self.content = content
		self.config = config
	}
	
	open func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return content.count
	}
	
	open func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		guard let cell = tableView.dequeueReusableCell(withIdentifier: identifer) else {
			fatalError("TableView does not have identifier: \(identifer)")
		}
		
		let obj = content[(indexPath as NSIndexPath).row]
		
		config(obj, cell)
		
		return cell
	}
	
	open subscript(path: IndexPath) -> AnyObject? {
		guard (path as NSIndexPath).row < content.count else {
			return nil
		}
		
		return content[(path as NSIndexPath).row]
	}
}
