//
//  Request+responseJSON.swift
//  SynKit
//
//  Created by Maximilian Felgenhauer on 4/7/16.
//  Copyright © 2016 Maximilian Felgenhauer. All rights reserved.
//

import Foundation
import Alamofire
import PromiseKit

extension DataRequest {
	public func responseJson() -> Promise<Any> {
		return process([.status(200..<300), .contentType(["application/json"])]) { data in
			return try JSONSerialization.jsonObject(with: data)
		}
	}
	
	fileprivate func process<T>(_ expectations: [ApiExpection], process: @escaping (Data) throws -> T) -> Promise<T> {
		return Promise { fulfill, reject in
			self.response { (response) in
				if .debug == mode {
					debugPrint(response)
				}
				
				guard response.error == nil else {
					reject(response.error!)
					return
				}
				
				guard let res = response.response else {
					reject(NetworkApiError.noResponse)
					return
				}
				
				do {
					let expectations: [ApiExpection] = [.status(200..<300), .contentType(["application/json"])]
					try validateResponse(res, expectations: expectations)
				} catch let err {
					reject(err)
					return
				}
				
				let result: T
				do {
					result = try process(response.data ?? "".data(using: .utf8)!)
				} catch let err {
					reject(err)
					return
				}
				
				fulfill(result)
			}
		}
	}
}


public enum NetworkApiError: Error {
	case noResponse
	case decodeError(Error)
	case statusMismatch(actual: Int, expected: CountableRange<Int>)
	case contentMismatch(actual: String, expected: [String])
}


private enum ApiExpection {
	case status(CountableRange<Int>)
	case contentType([String])
}


private func validateResponse(_ response: HTTPURLResponse, expectations: [ApiExpection]) throws {
	for expectation in expectations {
		switch expectation {
		case let .status(range):
			if !range.contains(response.statusCode) {
				throw NetworkApiError.statusMismatch(actual: response.statusCode, expected: range)
			}
		
		case let .contentType(types):
			let contentType = response.contentType ?? ""
			if !types.contains(contentType) {
				throw NetworkApiError.contentMismatch(actual: contentType, expected: types)
			}
		}
	}
}


extension HTTPURLResponse {
	var contentType: String? {
		guard let contentType = allHeaderFields["Content-Type"] as? String else {
			return nil
		}
		
		let ct = contentType.components(separatedBy: ";")[0].trimmingCharacters(in: CharacterSet.whitespaces)

    return ct
	}
}
