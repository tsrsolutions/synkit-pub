//
//  Initialize.swift
//  SynKit
//
//  Created by Maximilian Felgenhauer on 4/7/16.
//  Copyright © 2016 Maximilian Felgenhauer. All rights reserved.
//

import Foundation
import CoreData

var mode = ApiMode.production
var rootContext: NSManagedObjectContext?

public func initialize(rootContext context: NSManagedObjectContext, mode m: ApiMode) {
	mode = m
	rootContext = context
}

public func createRootContext(_ dbName: String, model: NSManagedObjectModel) throws -> NSManagedObjectContext {
	let documents = FileManager.default.documentsDirectory
	
	let dbPath = documents.appendingPathComponent(dbName)
	let coord = NSPersistentStoreCoordinator(managedObjectModel: model)
	
	try coord.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: dbPath, options: [
		NSMigratePersistentStoresAutomaticallyOption: true,
		NSInferMappingModelAutomaticallyOption: true
		])
	
	let context = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
	context.persistentStoreCoordinator = coord
	context.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy
	return context
}

public enum ApiMode {
	case debug
	case production
}

private extension FileManager {
	var documentsDirectory: URL {
		get {
			return self.urls(for: .documentDirectory, in: .userDomainMask).first!
		}
	}
}

public func apiMode() -> ApiMode {
	return mode
}
