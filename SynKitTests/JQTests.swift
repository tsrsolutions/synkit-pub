//
//  JQTests.swift
//  SynKit
//
//  Created by Maximilian Felgenhauer on 9/19/16.
//  Copyright © 2016 Maximilian Felgenhauer. All rights reserved.
//

import XCTest
import SynKit

class JQTests: XCTestCase {
	
	func testCast() {
		AssertNoThrow(try query(json: "Hello").string())
		AssertNoThrow(try query(json: 1).integer())
		AssertNoThrow(try query(json: 1.5 as Float).float())
		AssertNoThrow(try query(json: ["id": 1]).object())
		AssertNoThrow(try query(json: [1, 2]).array())
	}
	
	func testErrors() {
		AssertThrows(expression: try query(json: "Hello").integer(), expected: JQError.typeMismatch(actual: String.self, expected: Int.self))
		AssertThrows(expression: try query(json: ["id": 1]).through(keys: "valid").bool(), expected: JQError.missingKey(key: "valid", object: ["id": 1]))
		AssertThrows(expression: try query(json: [1, 2, 3]).through(keys: 2).bool(), expected: JQError.typeMismatch(actual: Int.self, expected: Bool.self))
	}
	
	func testThrough() {
		if case let val? = AssertNoThrow(try query(json: ["id": 1]).through(keys: "id").integer()) {
			XCTAssertEqual(val, 1)
		}
	}
}

