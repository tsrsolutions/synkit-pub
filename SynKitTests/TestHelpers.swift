//
//  TestHelpers.swift
//  SynKit
//
//  Created by Maximilian Felgenhauer on 9/19/16.
//  Copyright © 2016 Maximilian Felgenhauer. All rights reserved.
//

import Foundation
import XCTest

func XCTAssertThrows<T>(_ expression: @autoclosure () throws -> T, _ message: String = "", file: StaticString = #file, line: UInt = #line, _ expected: (Error) -> Bool = { _ in true}) {
	do {
		let _ = try expression()
		XCTFail("No error to catch! - \(message)", file: file, line: line)
	} catch let err {
		if !expected(err) {
			XCTFail("Error does not match expected", file: file, line: line)
		}
	}
}

func AssertThrows<T, Err>(expression: @autoclosure () throws -> T, _ message: String = "", file: StaticString = #file, line: UInt = #line, expected: Err) where Err: Equatable {
	
	do {
		let _ = try expression()
		XCTFail("No error to catch! - \(message)", file: file, line: line)
	} catch let error {
		guard let err = error as? Err else {
			XCTFail("Error does not match the expected type", file: file, line: line)
			return
		}
		
		XCTAssertEqual(err, expected, file: file, line: line)
	}
}

@discardableResult
func AssertNoThrow<T>(_ expression: @autoclosure () throws -> T, _ message: String = "", file: StaticString = #file, line: UInt = #line) -> T?{
	do {
		return try expression()
	} catch let error {
		XCTFail("Caught error: \(error) - \(message)", file: file, line: line)
		
		return nil
	}
}

func XCTAssertNoThrow<T>(_ expression: @autoclosure () throws -> T, _ message: String = "", file: StaticString = #file, line: UInt = #line) {
	do {
		let _ = try expression()
	} catch let error {
		XCTFail("Caught error: \(error) - \(message)", file: file, line: line)
	}
}
